#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define BUFSIZE 1024

int main(int argc, char *argv[]) {

  int pipe_fds[2];
  char command1[BUFSIZE];
  char command2[BUFSIZE];

  if (argc != 5) {
    printf("Usage:\n ./pipeme <command1> <arg1> <command2> <arg2>\n");
    return 0;
  }

  memset(command1, 0, BUFSIZE);
  sprintf(command1, "%s %s", argv[1], argv[2]);

  memset(command2, 0, BUFSIZE);
  sprintf(command2, "%s %s", argv[3], argv[4]);

  if (pipe(pipe_fds) == -1) {
    perror("pipe()");
    return 1;
  }

  int pid = fork();
  if (pid == -1) {
    perror("fork()");
    return 1;
  } else if (pid == 0) {
    // Child
    close(pipe_fds[1]);          /* Close unused write end */
    dup2(pipe_fds[0], 0);
    system(command2);
    close(pipe_fds[0]);
  } else {
    // Parent
    close(pipe_fds[0]);
    dup2(pipe_fds[1], 1);
    system(command1);
    close(pipe_fds[1]);          /* Close unused write end */
  }

  return 0;
}
