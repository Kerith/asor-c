#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFSIZE 256

int main(int argc, char **argv) {

  int p1, p2, hp;
  char buffer[BUFSIZE];
  fd_set rfds;
  int ret;

  while (1) {

    if((p1 = open(argv[1], O_RDWR | O_NONBLOCK)) == -1) {
      perror("open()");
      return 1;
    }
    if((p2 = open(argv[2], O_RDWR | O_NONBLOCK)) == -1) {
      perror("open()");
      return 1;
    }

    hp = (p1 > p1) ? p1 : p2;

    FD_ZERO(&rfds);
    FD_SET(p1, &rfds);
    FD_SET(p2, &rfds);

    ret = select(hp + 1, &rfds, NULL, NULL, NULL);

    if (ret == -1) {
      perror("select()");
      return 1;
    } else if (ret) {
      if (FD_ISSET(p1, &rfds)) {
        printf("Reading from pipe 1:\n");
        while(read(p1, &buffer, BUFSIZE) > 0){
					printf("%s", buffer);
        }
        printf("\nDone with pipe 1\n");
      } else {
        printf("Reading from pipe 2:\n");
        while(read(p2, &buffer, BUFSIZE) > 0){
					printf("%s", buffer);
        }
        printf("Done with pipe 2\n");
      }
      printf("\n");
    }

    close(p1);
    close(p2);
  }
}
