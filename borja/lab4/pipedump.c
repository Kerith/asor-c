#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

void print_usage() {
  printf("pipedump <stuff to dump>\n");
}

int main(int argc, char **argv) {

  if (argc != 2) {
    print_usage();
    return 0;
  }

  int pipe_fd = open("/home/blorente/pipe", O_WRONLY);
  if (pipe_fd == -1) {
    perror("open()");
    return 1;
  }

  int written = write(pipe_fd, argv[1], strlen(argv[1]));
  if (written == -1) {
    perror("write()");
    return 1;
  }
  written = write(pipe_fd, "\n", 3);
  if (written == -1) {
    perror("write()");
    return 1;
  }

  printf("Dumped to pipe: %s\n", argv[1]);

  return 0;
}
