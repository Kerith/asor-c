#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>

#define BUF_SIZE 256

int
main(int argc, char *argv[])
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s;
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len;
    ssize_t nread;

    fd_set rfds;
    int selret;

    char command;
    time_t curtime;
    char timemsg[BUF_SIZE];
    struct tm *nicetime;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s addr port\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
    hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
    hints.ai_protocol = 0;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(argv[1], argv[2], &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                rp->ai_protocol);
        if (sfd == -1)
            continue;

        if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break;                  /* Success */

        close(sfd);
    }

    if (rp == NULL) {               /* No address succeeded */
        fprintf(stderr, "Could not bind\n");
        exit(EXIT_FAILURE);
    }

    freeaddrinfo(result);           /* No longer needed */

    /* Read datagrams and echo them back to sender */

    while (1) {
      char host[NI_MAXHOST], service[NI_MAXSERV];
      peer_addr_len = sizeof(struct sockaddr_storage);

      FD_ZERO(&rfds);
      FD_SET(sfd, &rfds);
      FD_SET(0, &rfds);

      selret = select(sfd + 1, &rfds, NULL, NULL, NULL);
      if (selret == -1) {
        perror("select()");
        return 1;
      } else if (selret) {
        if (FD_ISSET(sfd, &rfds)) {
          nread = recvfrom(sfd, &command, 1, 0, (struct sockaddr *) &peer_addr, &peer_addr_len);
          s = getnameinfo((struct sockaddr *) &peer_addr,
          peer_addr_len, host, NI_MAXHOST,
          service, NI_MAXSERV, NI_NUMERICSERV);
          if (s == 0) {
            printf("Received command %c from %s:%s\n", command, host, service);
          } else {
            fprintf(stderr, "getnameinfo: %s\n", gai_strerror(s));
          }
        } else {
          nread = read(0, &command, 1);
          char nl;
          read(0, &nl, 1);
        }
        if (nread == -1) {
          continue;
        }
      }

      curtime = time(NULL);
      nicetime = localtime(&curtime);
      memset(timemsg, 0, BUF_SIZE);

      if (command == 'q') {
        break;
      } else if (command == 't') {
        sprintf(timemsg, "%i:%i:%i",
          nicetime->tm_hour, nicetime->tm_min, nicetime->tm_sec);
        if (sendto(sfd, timemsg, BUF_SIZE, 0, (struct sockaddr *) &peer_addr, peer_addr_len) != BUF_SIZE)
            fprintf(stderr, "Error sending response\n");
      } else if (command == 'd') {
        sprintf(timemsg, "%i-%i-%i",
          nicetime->tm_year + 1900, nicetime->tm_mon + 1, nicetime->tm_mday);
        if (sendto(sfd, timemsg, BUF_SIZE, 0, (struct sockaddr *) &peer_addr, peer_addr_len) != BUF_SIZE)
            fprintf(stderr, "Error sending response\n");
      }
    }

    return 0;
}
