#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>

#define MAX_HOST 1024
#define MAX_SERV 1024

void print_usage() {
  printf("addrinfo <host>\n");
}

int main(int argc, char **argv) {
  int s;

  struct addrinfo hints;
  struct addrinfo *result, *rp;

  if (argc != 2) {
    print_usage();
    return 0;
  }

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
  hints.ai_socktype = 0; /* Datagram socket */
  hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
  hints.ai_protocol = 0;          /* Any protocol */
  hints.ai_canonname = NULL;
  hints.ai_addr = NULL;
  hints.ai_next = NULL;

  // Get the address info for this node
  // First argument is the node (localhost, www.google.com...), second one is the port
  s = getaddrinfo(argv[1], NULL, &hints, &result);

  if (s != 0) {
    printf("getaddrinfo: %s\n",  gai_strerror(s));
    return 1;
  }

  // Iterate over every posible result
  for(rp = result; rp != NULL; rp = rp->ai_next) {
    struct sockaddr *addr = rp->ai_addr;
    socklen_t addrlen = rp->ai_addrlen;
    char hbuf[MAX_HOST], sbuf[MAX_SERV];

    if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,
            sizeof(sbuf), NI_NUMERICHOST) == 0) {
      //printf("%s\n", rp->ai_canonname);
      printf("host=%s\n", hbuf);
      printf("Socket type: %i\n", rp->ai_socktype);
      printf("Address family: %i\n", rp->ai_family);
    }

  }

}
