Call the server
./udpserver localhost 7777
./udpserver ip6-localhost 7777
./udpserver ::1 7777

Client
./udpclient localhost 7777 t
./udpclient ip6-localhost 7777 t
./udpclient ::1 7777 t

NC
nc -u localhost 7777
nc -u ip6-localhost 7777
nc -u ::1 7777
