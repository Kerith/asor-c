#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#define BUFSIZE 1024

int main(int argc, char const *argv[]) {
  int parent_pid;
  int parent_pgid;
  int parent_sid;
  struct rlimit file_limit;
  char cwd[BUFSIZE];

  parent_pid = getppid();
  parent_pgid = getpgid(parent_pid);
  parent_sid = getsid(parent_pid);

  if (getrlimit(RLIMIT_NOFILE, &file_limit)) {
    perror("ERROR! ");
    return -1;
  }

  getcwd(cwd, BUFSIZE);

  printf("|  PID| PGID|  SID|    Max FD|                                               CWD|\n");
  printf("|%5i|%5i|%5i|%10li|%50s|\n", parent_pid, parent_pgid, parent_sid, file_limit.rlim_max, cwd);

  return 0;
}
