#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFSIZE 1024

void print_usage() {
  printf("Usage: execute <-e or -s> <program> [arguments]\n");
}

int main(int argc, char const *argv[]) {
  if (argc < 3) {
    print_usage();
    return 0;
  }

  char command[BUFSIZE];
  int argsize = argc - 2;
  int i;
  char *head = command;
  for (i = 0; i < argsize; i++) {
    head += sprintf(head, argv[i + 2]);
    head += sprintf(head, " ");
  }

  printf("%s\n", command);

  if (argv[1][1] == 'e') {
    execl("/bin/sh", "sh", "-c", command, (char *) 0);
  } else if (argv[1][1] == 's') {
    system(command);
  } else {
    print_usage();
  }
  return 0;
}
