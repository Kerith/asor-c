#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#define BUFSIZE 1024

int display_info() {
  int parent_pid;
  int parent_pgid;
  int parent_sid;
  struct rlimit file_limit;
  char cwd[BUFSIZE];

  parent_pid = getppid();
  parent_pgid = getpgid(parent_pid);
  parent_sid = getsid(parent_pid);

  if (getrlimit(RLIMIT_NOFILE, &file_limit)) {
    perror("ERROR! ");
    return -1;
  }

  getcwd(cwd, BUFSIZE);

  printf("| PPID| PGID|  SID|    Max FD|                                               CWD|\n");
  printf("|%5i|%5i|%5i|%10li|%50s|\n", parent_pid, parent_pgid, parent_sid, file_limit.rlim_max, cwd);
  return 0;
}

int main(int argc, char const *argv[]) {
  int pid;

  pid = fork();
  if (pid == -1) {
    perror("Error creating child");
    return 1;
  } else if (pid == 0) { // DAEMON LOGIC
    printf("DAEMON IN\n");

    pid = setsid();
    if (chdir("/tmp") != 0) {
      perror("FAILURE!!");
      return 1;
    }
    if (display_info()) {
      perror("FAILURE!!");
      return 1;
    }

    printf("DAEMON OUT\n");
  } else { // Parent logic
    printf("PARENT IN\n");
    sleep(2);
    printf("PARENT OUT\n");
  }

  return 0;
}

/*
If parent exits before the child can get the info,
the PPID changes to that of /usr/lib/systemd/systemd daemon
*/
