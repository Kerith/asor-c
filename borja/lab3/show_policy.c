#include <stdio.h>
#include <errno.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>

int main (int argc, char **argv) {
  int policy;
  int priority;

  policy = sched_getscheduler(0);
  if (policy == -1) {
    perror("ERROR! ");
    return 1;
  }

  // Since getpriority can return -1,
  // we need to look directly into errno
  errno = 0;
  priority = getpriority(PRIO_PROCESS, 0);
  if (errno == -1) {
    perror("ERROR! ");
    return 1;
  }

  if (policy == SCHED_OTHER) {
    printf("Standard Round-Robin time-sharing policy.\n");
    printf("Priority value is: %i\n", priority);
    printf("Max priority is -20\n");
    printf("Min priority is 19\n");
  } else if (policy == SCHED_FIFO) {
    printf("FIFO Preemptive policy.\n");
    printf("Priority value is: %i\n", priority);
    printf("Max priority is 99\n");
    printf("Min priority is 0\n");
  } else if (policy == SCHED_RR) {
    printf("Quantized Round-Robin policy.\n");
    printf("Priority value is: %i\n", priority);
    printf("Max priority is 99chrt\n");
    printf("Min priority is 0\n");
  } else {
    printf("Unable to identify your policy.\n");
  }

  return 0;
}
