#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

// Must be called with "env SLEEP_SECS=5 ./blocky"

int main(int argc, char const *argv[]) {

  int my_pid = getpid();
  printf("My pid is: %i\n", my_pid);

  sigset_t blocked_set;
  sigset_t old_set;
  sigset_t pending_set;

  if (sigemptyset(&blocked_set) == -1) {
  		perror("Failed to create the empty set");
  }

  if(sigaddset(&blocked_set, SIGINT) == -1) {
		perror("Failed to add SIGINT");
    return 1;
	}

  if (sigaddset(&blocked_set, SIGTSTP) == -1) {
		perror("Failed to add SIGTSTP");
    return 1;
	}

  if (sigprocmask(SIG_BLOCK, &blocked_set, &old_set) == -1) {
    perror("FAILURE!!");
    return 1;
  }

  char * sleep_str = getenv("SLEEP_SECS");
  if (sleep_str == NULL) {
    printf("FAILURE!!\n");
    return 1;
  }
  int sleep_secs = 0;
  sscanf(sleep_str, "%i", &sleep_secs);
  printf("Sleep for %is\n", sleep_secs);

  sleep(sleep_secs);

  printf("Recover the signals\n");

  if (sigpending(&pending_set) == -1) {
    perror("sigpending()");
    return 1;
  }

  printf("Pending signals:\n");

  int member = sigismember(&pending_set, SIGINT);
  if (member == -1) {
    perror("FAILURE!!");
    return 1;
  }
  if (member == 1) {
    printf("SIGINT\n");
  }
  member = sigismember(&pending_set, SIGTSTP);
  if (member == -1) {
    perror("FAILURE!!");
    return 1;
  }
  if (member == 1) {
    printf("SIGTSTP\n");
  }

  if (sigprocmask(SIG_SETMASK, &old_set, &blocked_set) == -1) {
    perror("FAILURE!!");
    return 1;
  }
  return 0;
}
