# ASOR Review session

- Review the theory
- 3:30 the theory part, 5:00 practice part
- More Network theory questions than OS! Practice part will have more OS questions.

## 2015 Theory

1) Type is multicast, scope is local link. IF they asked about the format, we shoud also say ff02::...
2) Remember to add 1 to each distance if it's less than the one in the table! 
	REmember to update the distances! If we had an entry such as (dist: 4, path: A), and from A we recieve (dist:5, path: B), we have to update to (dist:6 (5 + 1), path: A). 
	Remember that if the distances are the same through 2 paths, we keep the old one!
3)  Say that the compulsory attributes are the ones that EVERY IMPLEMENTATION should have, and that the well-known ones are those defined in the standard.
	The AS_PATH attribute is used to check for loops in the path. (more required, didn't get it).

4) Explain all three, not a formula needed, just the basic gist of it. Jacobson's Algorithm, Jacobson's/Karel's, and Karn's. One combines the smooth average, the other smooth avg. and the std. deviaion, and another one doesn't take into account the RTT of the retransmitted elements, and in this case the RTO is doubled. HOWEVER, in TCP only Karn's is used, so we could just answer that.

5) Depends on the signal, it just does the default action associated to the signal.

6) There are 2 sockets, one listening for connections, and this creates another socket to handle the connection. Then the TCP server can fork, and the listener process can go back to listening. 

7) Fifos or sockets.

## Theory 2014

1) 
	a) Type link local unicast, scope link local 
	b) Global unicast, global 
	c) Unique local unicast, global -> Tricky!
	d) Multicast, Link-local (all nodes address)
	e) Multicast, Link-local (scope is the last 2 in ff12, the 1 means transient address, 0 would mean permanent address as in before).

2) Problems: SOmeone can track you by the suffix of your address. This is only a problem for clients, users, not for servers, because they won't move it around.
3) Mention the differences:
	- ARP uses the broadcast ethernet address, and IPv6 uses the solicited node multicast address.
	- 
4) RIP Is important!!!!
5) 3 types:
	- Intra-area routers. Connect devices in an area.
	- Area-border. Connect different borders of the AS.
	- Autonomous system border routers. Connect ASes to ASes.
6) **Stub** (ISP, every connection passing through has this as an origin or destination), **multihomed** (A tier 3 autonomous system that is connected to more than one transits, but it doesnt allow transit itself), **transit** (ISPs are connected to it, traffic can go through it, that means it's neither an origin nor a destination). BGP slides.
7) Note the LIGHT congestion. It's enough to draw it! The answer is FAST RECOVERY. Detected when we receiver 3 dupped acks. We perform congestion avoidance (increase congestion window linearly, and drop to half every 3rd consecutive ack), which comes after the slow start and before the stable period.
8) ??